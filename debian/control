Source: vlfeat
Section: science
Priority: optional
Build-Depends: debhelper (>= 11), dh-exec, octave-dev, dh-octave,
               transfig, ghostscript, librsvg2-bin, texlive-latex-base, imagemagick,
               python3, python3-pygments, doxygen, rsync, groff
Build-Conflicts: graphicsmagick-imagemagick-compat
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Dima Kogan <dkogan@debian.org>
Standards-Version: 4.1.3
Homepage: http://www.vlfeat.org
Vcs-Git: https://salsa.debian.org/science-team/vlfeat.git
Vcs-Browser: https://salsa.debian.org/science-team/vlfeat

Package: libvlfeat1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Computer vision library focussing on visual features and clustering
 VLFeat implements popular computer vision algorithms including MSER, k-means,
 hierarchical k-means, agglomerative information bottleneck, and quick shift. It
 is written in C for efficiency and compatibility, with interfaces in GNU octave
 for ease of use, and detailed documentation throughout

Package: libvlfeat-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, libvlfeat1 (= ${binary:Version})
Recommends: libvlfeat-doc
Description: Computer vision library focussing on visual features and clustering
 VLFeat implements popular computer vision algorithms including MSER, k-means,
 hierarchical k-means, agglomerative information bottleneck, and quick shift. It
 is written in C for efficiency and compatibility, with interfaces in GNU octave
 for ease of use, and detailed documentation throughout
 .
 Development files

Package: libvlfeat-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, libjs-mathjax
Replaces: libvlfeat-dev (<< 0.9.21+dfsg0-1)
Breaks: libvlfeat-dev (<< 0.9.21+dfsg0-1)
Description: Computer vision library focussing on visual features and clustering
 VLFeat implements popular computer vision algorithms including MSER, k-means,
 hierarchical k-means, agglomerative information bottleneck, and quick shift. It
 is written in C for efficiency and compatibility, with interfaces in GNU octave
 for ease of use, and detailed documentation throughout
 .
 Documentation

Package: octave-vlfeat
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, libvlfeat1 (>= ${binary:Version}),
         ${octave:Depends}
Recommends: libvlfeat-doc
Description: Computer vision library focussing on visual features and clustering
 VLFeat implements popular computer vision algorithms including MSER, k-means,
 hierarchical k-means, agglomerative information bottleneck, and quick shift. It
 is written in C for efficiency and compatibility, with interfaces in GNU octave
 for ease of use, and detailed documentation throughout. Note that some of the
 upstream library is under patent protection, and those components (SIFT for
 instance) are not included in this package
 .
 GNU Octave interface
